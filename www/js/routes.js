angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

  .state('tabs.lists', {
    url: '/lists',
    views: {
      'tab1': {
        templateUrl: 'templates/lists.html',
        controller: 'listsCtrl'
      }
    }
  })

  .state('tabs.needs', {
    url: '/needs',
    views: {
      'tab2': {
        templateUrl: 'templates/needs.html',
        controller: 'needsCtrl'
      }
    }
  })

  .state('tabs.achievements', {
    url: '/achievements',
    views: {
      'tab3': {
        templateUrl: 'templates/achievements.html',
        controller: 'achievementsCtrl'
      }
    }
  })

  .state('tabs', {
    url: '/tabs',
    templateUrl: 'templates/tabs.html',
    abstract:true
  })

  .state('tabs.observations', {
    url: '/observations',
    views: {
      'tab4': {
        templateUrl: 'templates/observations.html',
        controller: 'observationsCtrl'
      }
    }
  })

  .state('tabs.birdIdentification', {
    url: '/bird-identification',
    views: {
      'tab5': {
        templateUrl: 'templates/birdIdentification.html',
        controller: 'birdIdentificationCtrl'
      }
    }
  })

$urlRouterProvider.otherwise('/tabs/lists')

  

});