angular.module('app.controllers', [])
  
.controller('menuCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {

}])
   
.controller('listsCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {

    var lists =[
                 {
                   location: 'California',
                   timeframe: 'Year',
                   count: '1354'
                },
                {
                   location: 'California',
                   timeframe: 'Week',
                   count: '92'
                },
                {
                   location: 'Yard',
                   timeframe: 'Month',
                   count: '150'
                },
                {
                   location: 'San Francisco',
                   timeframe: 'Day',
                   count: '48'
                }
                ];

    $scope.lists = lists;
    
}])
   
.controller('needsCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {
    
    var needs =[
                 {
                   species: 'Prothonotary Warbler',
                   location: 'Madison',
                   lists: ['Year', 'Month'],
                   observers: ['Joh Doe', 'Jane Doe'],
                   lastObserved: '2016-09-29',
                   notes: ['John Doe: Very pretty.'],
                   image: 'img/prothonotary warbler.JPG'
                },
                {
                   species: 'Veery',
                   location: 'Nashville-Davidson',
                   lists: ['Year', 'Month'],
                   observers: ['Joh Doe', 'Jane Doe'],
                   lastObserved: '2016-09-29',
                   notes: ['John Doe: Very pretty.'],
                   image: 'img/veery.JPG'
                },
                {
                   species: 'Bay-breasted Warbler',
                   location: 'Columbus',
                   lists: ['Year', 'Month'],
                   observers: ['Joh Doe', 'Jane Doe'],
                   lastObserved: '2016-09-29',
                   notes: ['John Doe: Very pretty.'],
                   image: 'img/bay-breasted warbler.JPG'
                },
                {
                   species: 'Brown Creeper',
                   location: 'Greensboro',
                   lists: ['Year', 'Month'],
                   observers: ['Joh Doe', 'Jane Doe'],
                   lastObserved: '2016-09-29',
                   notes: ['John Doe: Very pretty.'],
                   image: 'img/brown creeper.JPG'
                }
                ];

    $scope.needs = needs;

}])
   
.controller('achievementsCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {

    var achievements =[
                 {
                   id: 0,
                   name: '100 Species Observed',
                   desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ullamcorper maximus molestie. Phasellus accumsan ornare nulla vel vehicula.',
                   date: '2016-09-30',
                   icon: 'binoculars'
                },
                {
                   id: 1,
                   name: 'Raptor City',
                   desc: 'Vestibulum consectetur lacinia leo, ut fermentum velit eleifend quis. Integer imperdiet feugiat convallis.',
                   date: '2015-05-30',
                   icon: 'thumbs-up'
                },
                {
                   id: 2,
                   name: 'Knock knock',
                   desc: 'Morbi ac diam ullamcorper, convallis urna quis, sagittis dolor. Quisque malesuada odio non tortor lacinia vestibulum. Phasellus efficitur purus vitae odio gravida, at cursus dui iaculis.',
                   date: '2014-12-15',
                   icon: 'themeisle'
                },
                {
                   id: 3,
                   name: 'Chickadee-dee-dee',
                   desc: 'Etiam sagittis pretium justo non varius. Mauris ac consequat nisl, ac lacinia turpis. Cras iaculis, est eget euismod posuere, neque nibh lacinia enim, eget sagittis ligula sem cursus ligula.',
                   date: '2016-09-03',
                   icon: 'pied-piper-alt'
                }
                ];
    
    $scope.achievements = achievements;

    var achievementsInProgress =[
                 {
                   name: '100 Species Observed',
                   desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ullamcorper maximus molestie. Phasellus accumsan ornare nulla vel vehicula.',
                   progress: '90',
                   icon: 'binoculars'
                },
                {
                   name: 'Raptor City',
                   desc: 'Vestibulum consectetur lacinia leo, ut fermentum velit eleifend quis. Integer imperdiet feugiat convallis.',
                   progress: '56',
                   icon: 'thumbs-up'
                },
                {
                   name: 'Knock knock',
                   desc: 'Morbi ac diam ullamcorper, convallis urna quis, sagittis dolor. Quisque malesuada odio non tortor lacinia vestibulum. Phasellus efficitur purus vitae odio gravida, at cursus dui iaculis.',
                   progress: '34',
                   icon: 'themeisle'
                },
                {
                   name: 'Chickadee-dee-dee',
                   desc: 'Etiam sagittis pretium justo non varius. Mauris ac consequat nisl, ac lacinia turpis. Cras iaculis, est eget euismod posuere, neque nibh lacinia enim, eget sagittis ligula sem cursus ligula.',
                   progress: '16',
                   icon: 'pied-piper-alt'
                }
                ];
    
    $scope.achievementsInProgress = achievementsInProgress;

}])
      
.controller('observationsCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {

    var observations =[
                     {
                       species: 'Yellow-rumped Warbler',
                       location: 'Minneapolis',
                       datetime: '2016-09-29 09:00',
                       count: 20,
                       notes: 'Very pretty.'
                    },
                    {
                       species: 'Magnolia Warbler',
                       location: 'New Orleans',
                       datetime: '2016-09-15 13:00',
                       count: 2,
                       notes: 'Very pretty.'
                    },
                    {
                       species: 'Black-capped Chickadee',
                       location: 'Plano',
                       datetime: '2014-01-12 19:00',
                       count: 30,
                       notes: 'Very pretty.'
                    },
                    {
                       species: 'Boreal Chickadee',
                       location: 'Scottsdale',
                       datetime: '2015-05-01 16:00',
                       count: 5,
                       notes: 'Very pretty.'
                    }
                    ];
    
    $scope.observations = observations;
    
}])
   
.controller('birdIdentificationCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {

    var speciesList =[
             {
               name: 'Prothonotary Warbler',
               desc: 'The prothonotary warbler is 13 cm (5.1 in) long and weighs 12.5 g (0.44 oz). It has an olive back with blue-grey wings and tail, yellow underparts, a relatively long pointed bill and black legs. The adult male has a bright orange-yellow head. Females and immature birds are duller and have a yellow head. In flight from below, the short, wide tail has a distinctive two-toned pattern, white at the base and dark at the tip.',
               image: 'img/prothonotary warbler.JPG'
            },
            {
               name: 'Veery',
               desc: 'This species measures 16–19.5 cm (6.3–7.7 in) in length. Its mass is 26–39 g (0.92–1.38 oz), exceptionally up to 54 g (1.9 oz). The wingspan averages 28.5 cm (11.2 in). Each wing measures 8.9–10.4 cm (3.5–4.1 in), the bill measures 1.2–1.9 cm (0.47–0.75 in) and the tarsus is 2.7–3.25 cm (1.06–1.28 in). The veery shows the characteristic underwing stripe of Catharus thrushes. Adults are mainly light brown on the upperparts. The underparts are white; the breast is light tawny with faint brownish spots. Veeries have pink legs and a poorly defined eye ring.',
               image: 'img/veery.JPG'
            },
            {
               name: 'Bay-breasted Warbler',
               desc: 'This species is closely related to blackpoll warbler, but this species has a more southerly breeding range and a more northerly wintering area. The summer male bay-breasted warblers are unmistakable. They have grey backs, black faces, and chestnut crowns, flanks and throats. They also boast of bright yellow neck patches, and their underparts are white. They have two white wing bars, as well.',
               image: 'img/bay-breasted warbler.JPG'
            },
            {
               name: 'Brown Creeper',
               desc: 'Adults are brown on the upper parts with light spotting, resembling a piece of tree bark, with white underparts. They have a long thin bill with a slight downward curve and a long stiff tail used for support as the bird creeps upwards. The male creeper has a slightly larger bill than the female. The brown creeper is 11.7–13.5 cm (4.6–5.3 in) long. Its voice includes single very high pitched, short, often insistent, piercing calls; see, or swee. The song often has a cadence like; pee pee willow wee or see tidle swee, with notes similar to the calls.',
               image: 'img/brown creeper.JPG'
            }
            ];

    $scope.speciesList = speciesList;
    
}])

.filter('joinBy', function () {
    return function (input,delimiter) {
        return (input || []).join(delimiter || ',');
    };
})

 